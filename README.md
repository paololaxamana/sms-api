# SMS API 

SMS api using Semaphore

Used Semaphore as a SMS gateway for VentiiEats 

* Only limited to sending text using Django Framework
* 

INITIAL TEST: 

1. Make an sms.py file in project root folder
2. Setup your own app in the api's website: https://semaphore.co/dashboard
3. Get your API key, declare it in the start of you .py file as:

    apikey = 'apikey' (with quotation marks)
    
4. Sender name will be by default, Semaphore. Even if default, for initial testing declare it as semaphore still.

    sendername = 'SEMAPHORE'
    
5. Declare the parameters as the ff: apikey, sendername, message, number

    params = (
            ('apikey', apikey),
            ('sendername', sendername'),
            ('message', 'hello from ___),
            ('number', number)
    )
    
6. Send a POST request to the api's link, can be seen in their documentation.  (https://semaphore.co/docs)
    

    r = requests.post('https://api.semaphore.co/api/v4/messages', params = params)
    
7. Use json to get errors info (?)


    data = r.json()
    print(data) [PRINT ERRORS can be seen in terminal]
    print(r)    [r prints the status of the message that you just sent]
    
    
    
After each HTTP POST request, you will receive a JSON response containing the following parameters for each message:

PARAMETER	DESCRIPTION

message_id	The unique identifier for your message

user_id	The unique identifier for the user who sent the message

user	The email address of the user who sent the message

account_id	The unique identifier of the account the message was sent by

account	The name of the account the message was sent by

recipient	The phone number the message was sent to

message	The body of the message that was sent

sender_name	The Sender Name the message was sent from

network	The recipient phone number's network

[STATUS (diff types)]

*Queued*	The message is queued to be sent to the network

*Pending*	The message is in transit to the network

*Sent*	The message was successfully delivered to the network

*Failed*	The message was rejected by the network and is waiting to be refunded

*Refunded*	The message has been refunded and your account balance has been adjusted

*type*	If the message was sent to one number the type will be "single". If the message was sent to more than "one" number the type will be "bulk". If the message was sent to the priority queue the type will be "priority"

*source*	The source of the message. For messages sent via API, the source will be "api". For messages sent through the web tool, the source will be "webtool". For messages sent through the bulk tool, the source will be "csv"

*created_at*	The timestamp the message was created

*updated_at*	The timestamp the message was last updated
